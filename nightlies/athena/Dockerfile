# Image providing Athena

# Make the base image configurable.
#ARG BASEIMAGE=registry.cern.ch/desilva/atlas-dev-almalinux9:20240806
ARG BASEIMAGE=registry.cern.ch/atlasadc/atlas-grid-almalinux9 
FROM ${BASEIMAGE}

ARG TIMESTAMP="2022-12-01T2223"
ARG RELEASES="main--testContainers"
ARG RELEASE=22.2.101
ARG PROJECT=Athena
ARG BINARY_TAG=x86_64-el9-gcc13-opt
ARG PLATFORM=${BINARY_TAG}
ARG PACKAGE=${PROJECT}_${RELEASE}_${BINARY_TAG}
ARG NIGHTLYVER=${RELEASES}/${PROJECT}/${BINARY_TAG}/${TIMESTAMP}

ARG WORKDIR=/home/atlas

# Helper variable(s).
ARG ATHENARELEASE=Athena_${RELEASE}_${PLATFORM}

# Contact information
LABEL maintainer="Atlas.Nightlybuild@cern.ch"

# Helper environment variables for the image.
ENV AtlasProject Athena
ENV AtlasVersion ${RELEASE}
ENV CMTCONFIG ${PLATFORM}
ENV RELEASES=$RELEASES
ENV RELEASE=$RELEASE
ENV PROJECT=$PROJECT
ENV BINARY_TAG=$BINARY_TAG

# Perform the installation as root.
USER root
WORKDIR /root

# 1. Install all "ayum packages"
# 2. Clean up to reduce the image size
COPY repo_config /root/repo_config.sh
RUN if [ ! -z "$TIMESTAMP" ] ; then source /root/repo_config.sh ; fi
RUN dnf --refresh -y install $PACKAGE --nogpgcheck && \
    dnf --refresh -y install libuuid-devel && \
    dnf clean all 

RUN rm -rf /opt/lcg/gcc/6.2.0 && \
    rm -rf /opt/lcg/gcc/8.2* && \
    rm -rf /opt/lcg/gcc/11.1*

# Remove the environment setup script(s) that are more trouble than they
# are worth.
RUN rm -f /etc/profile.d/color* /etc/profile.d/zzz_hepix.*

# Add the release setup script to the home directory, and its
# "usage instructions".
COPY release_setup.sh /
RUN echo "echo Container created on \`date -u\`" >> /release_setup.sh
COPY motd /etc/

# create user for installation purposes and for running without setupATLAS -c.
# note setupATLAS -c will not run as this user but as the user on the host.
#
RUN useradd -m -d /atlas -K UMASK=0002 -s /usr/bin/bash atlas 

# Set up a soft link for release_setup.sh inside the /home/atlas directory.
RUN cp -a  /release_setup.sh /atlas/release_setup.sh
RUN chown atlas:atlas /atlas/release_setup.sh
##RUN echo "source /release_setup.sh" > /etc/profile.d/atlas.sh

# Default user to run as if ALRB is not used.
USER atlas

# you can run a script to install things as atlas (no root privileges)
#COPY files/installAsUser.sh /atlas/installAsUser.sh
#RUN source /atlas/installAsUser.sh

# the dir that users end up when not using setupATLAS -c
WORKDIR /atlas

# Start the image with BASH by default, after having printed the message
# of the day.
CMD cat /etc/motd && /bin/bash
