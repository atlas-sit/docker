#!/bin/sh
project_sanity_test=''
[[ "${PROJECT_SANITY_TEST}" != "" ]] && project_sanity_test="${PROJECT_SANITY_TEST}"
while [ $# -ne 0 ]; do
    case $1 in
         --project) project_sanity_test=$2; shift;;
         -* | --*)         echo "image_build_and_push_to_harbor.sh: Error: invalid option"; exit 1;;
    esac
    shift
done
if [ "${project_sanity_test}" = "" ]; then
    echo "container_sanity_test: Error: project is not defined neither in the enviroment nor in command line"
    exit 1
fi
echo "====================================================================="
echo "===This is the logfile of a ${project_sanity_test} container sanity test"
if [ "${project_sanity_test}" != "AnalysisBase" ]; then
echo "===It includes simple checks: AthExHelloWorld/HelloWorldOptions"
echo "===-------------------------------- AthExHelloWorld/HelloWorldConfig.py"
echo "===-------------------------------- Tools/PyUtils/test/test_RootUtils.py"
echo "===-------------------------------- xAODEventFormatCnv.TestWriteEventFormat"
fi
echo "====================================================================="

source /release_setup.sh
echo "===RUNTIME ENVIRONMENT" 
printenv
echo "===RUNTIME ENVIRONMENT printout end"

[[ -d /tmp/container_tests_workdir ]] && rm -rf /tmp/container_tests_workdir
mkdir -p /tmp/container_tests_workdir
cd /tmp/container_tests_workdir

if [ "${project_sanity_test}" = "AnalysisBase" ]; then
    st_HelloWorldOptions=0
    st_PyUtils=0
    st_EventFormatWrite=0
    st_HelloWorldConfig=0
    echo "===SKIPPING tests for project ${project_sanity_test}"
else
    echo "===STARTING AthExHelloWorld/HelloWorldOptions test"
    athena.py AthExHelloWorld/HelloWorldOptions.py; st_HelloWorldOptions=$?
    [[ "$st_HelloWorldOptions" -eq 0 ]] && echo "===Test HelloWorldOptions PASSED" || echo "===Test HelloWorldOptions FAILED HelloWorldOptions"
    echo "===STARTING HelloWorldConfig test"
    athena.py --config-only=ca.pkl AthExHelloWorld/HelloWorldConfig.py; st_HelloWorldConfig=$?
    [[ "$st_HelloWorldConfig" -eq 0 ]] && echo "===Test HelloWorldConfig PASSED" || echo "===Test HelloWorldConfig FAILED ${st_HelloWorldConfig}"
    echo "===STARTING Tools/PyUtils test"
    eval python "\$${project_sanity_test}_DIR/src/Tools/PyUtils/test/test_RootUtils.py"; st_PyUtils=$?
    [[ "$st_PyUtils" -eq 0 ]] && echo "===Test Tools/PyUtils PASSED" || echo "===Test Tools/PyUtils FAILED $st_PyUtils"
    echo "===STARTING xAOD EventFormatWrite test"
    python -m xAODEventFormatCnv.TestWriteEventFormat; st_EventFormatWrite=$?
    [[ "$st_EventFormatWrite" -eq 0 ]] && echo "===Test xAOD EventFormatWrite PASSED" || echo "===Test xAOD EventFormatWrite FAILED $st_EventFormatWrite"
fi
if [ "$st_HelloWorldConfig" -ne 0 -o "$st_PyUtils" -ne 0 -o "$st_EventFormatWrite" -ne 0 -o "$st_HelloWorldOptions" -ne 0 ]; then
echo "Container sanity test: error:  the following tests failed:"
[[ "$st_HelloWorldConfig" -ne 0 ]] && echo "--- HelloWorldConfig failed with exit code $st_HelloWorldConfig"
[[ "$st_PyUtils" -ne 0 ]] &&	echo "--- PyUtils failed with exit code $st_PyUtils"
[[ "$st_EventFormatWrite" -ne 0 ]] &&	echo "--- EventFormatWrite failed with exit code $st_EventFormatWrite"
[[ "$st_HelloWorldOptions" -ne 0 ]] &&	echo "--- HelloWorldOptions failed with exit code $st_HelloWorldOptions"
exit 1
else
echo "Container sanity test PASSED"
exit 0
fi


    
