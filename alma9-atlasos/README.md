AlmaLinux OS 9 for ATLAS
=============

This configuration can be used to set up a base image that ATLAS analysis
and offline releases can be installed on top of. So that each image would
not have to duplicate the same base components.

During the build you can, and should choose the GCC version to be installed,
explicitly.

You can build a GCC 13 based image with the following:

```bash
docker build \
   -t gitlab-registry.cern.ch/atlas-sit/docker/alma9-atlasos:X.Y.Z-gcc13 \
   -t gitlab-registry.cern.ch/atlas-sit/docker/alma9-atlasos:X.Y.Z \
   -t gitlab-registry.cern.ch/atlas-sit/docker/alma9-atlasos:latest-gcc13 \
   -t gitlab-registry.cern.ch/atlas-sit/docker/alma9-atlasos:latest \
   --compress .
```

Images
------

You can find pre-built images on
[gitlab-registry.cern.ch/atlas-sit/docker/alma9-atlasos](https://gitlab.cern.ch/atlas-sit/docker/container_registry/17861).
