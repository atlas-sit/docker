DBRelease Images for the AtlasOffline/Athena/AthSimulation Projects.
======================================

This procedure can be used to add a DBRelease package to an image with an existing
full installation of AtlasOffline, Athena or AthSimulation. 

Move to the DBRelease folder
----
```bash
cd docker/dbrelease
```

Build the images
----
To install a given database release package (e.g., DBRelease-XXX.Y.Z) in an existing release image (e.g., registry.cern.ch/atlas/athsimulation:24.0.42), execute:

In an aarch64 host
```bash
podman build --platform linux/arm64 -t registry.cern.ch/atlas/athsimulation:24.0.42-XXX.Y.Z-aarch64 --build-arg BASEIMAGE=registry.cern.ch/atlas/athsimulation:24.0.42-aarch64 --build-arg DBRELEASE=XXX.Y.Z --compress --squash .
```

In an x86_64 host
```bash
podman build --platform linux/amd64 -t registry.cern.ch/atlas/athsimulation:24.0.42-XXX.Y.Z-x86_64 --build-arg BASEIMAGE=registry.cern.ch/atlas/athsimulation:24.0.42-x86_64 --build-arg DBRELEASE=XXX.Y.Z --compress --squash .
```

Build a multi-architecture manifest
----
```bash
podman manifest create registry.cern.ch/atlas/athsimulation:24.0.42-XXX.Y.Z registry.cern.ch/atlas/athsimulation:24.0.42-XXX.Y.Z-x86_64 registry.cern.ch/atlas/athsimulation:24.0.42-XXX.Y.Z-aarch64
```
