#!/bin/sh
set -e

echo $GITLABPASSWORD|docker login -u $GITLABUSER --password-stdin gitlab-registry.cern.ch

revision=${ATLAS_DOCKER_REV:-master}
analysis_release=${ANALYSIS_RELEASE:-21.2.2}
image_prefix=${ATLAS_DOCKER_IMGPREFIX:-gitlab-registry.cern.ch/atlas-sit/docker}


git clone https://:@gitlab.cern.ch:8443/atlas-sit/docker.git atlas-sit-docker
cd atlas-sit-docker
git checkout $revision


date_tag=$(date +%Y%m%d)
revision_tag=$(git rev-parse --short HEAD)

push_images=${ATLAS_DOCKER_PUSH:-false}

atlasos_img_name=${ATLAS_DOCKER_ATLASOSBASE:-""}

if [ -z "$atlasos_img_name" ];then
  cd slc6-atlasos
  atlasos_img_name="${image_prefix}/slc6-atlasos:${revision_tag}-${date_tag}"
  echo "::: building ${atlasos_img_name}"
  docker build -t $atlasos_img_name --compress --squash .

  echo "::: build of ${atlasos_img_name} done"
  if $ATLAS_DOCKER_PUSH;then
    docker push ${atlasos_img_name}
    echo "::: push of ${atlasos_img_name} done"
    cd ..
  fi
else
  echo "::: taking pre-defined image ${atlasos_img_name}"
fi

build_analysisbase=${BUILD_ANALYSISBASE:-false}
if $build_analysisbase;then
  cd slc6-analysisbase
  analysisbase_img_name="${image_prefix}/analysisbase:${analysis_release}-${revision_tag}-${date_tag}"



  echo "::: building ${analysisbase_img_name}"

  docker build -t $analysisbase_img_name\
          --build-arg RELEASE="$analysis_release" \
          --build-arg BASEIMAGE="$atlasos_img_name" \
          --compress \
          --squash .

  echo "::: build of ${analysisbase_img_name} done"
  if $ATLAS_DOCKER_PUSH;then
    docker push ${analysisbase_img_name}
    echo "::: push of ${analysisbase_img_name} done"
    cd ..
  fi
else
  echo "::: skip analysisbase"
fi


build_athanalysis=${BUILD_ATHANALYSIS:-false}
if $build_athanalysis;then
  cd slc6-athanalysis
  athanalysis_img_name="${image_prefix}/athanalysis:${analysis_release}-${revision_tag}-${date_tag}"
  echo "::: building ${athanalysis_img_name}"

  docker build -t $athanalysis_img_name\
        --build-arg RELEASE="$analysis_release" \
        --build-arg BASEIMAGE="$atlasos_img_name" \
        --compress \
        --squash .

  echo "::: build of ${athanalysis_img_name} done"
  cd ..
  if $ATLAS_DOCKER_PUSH;then
    docker push ${athanalysis_img_name}
    echo "::: push of ${athanalysis_img_name} done"
  fi
else
  echo "::: skip athanalysisbase"
fi
